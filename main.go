package main

import (
	"net/url"
	"os"

	"github.com/joho/godotenv"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	app := echo.New()
	err := godotenv.Load()
	if err != nil {
		app.Logger.Fatal("Error loading .env file")
	}

	url1, err := url.Parse(os.Getenv("UPSTREAM_URL"))
	if err != nil {
		app.Logger.Fatal(err)
	}

	targets := []*middleware.ProxyTarget{
		{
			URL: url1,
		},
	}

	app.Use(middleware.Proxy(middleware.NewRoundRobinBalancer(targets)))
	app.Logger.Fatal(app.Start(os.Getenv("SERVICE_PORT")))
}
